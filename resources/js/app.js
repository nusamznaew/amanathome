/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap.js');

import 'bootstrap';


window.Vue = require('vue');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

import VueEllipseProgress from 'vue-ellipse-progress';


Vue.use(VueEllipseProgress);
import VueTheMask from 'vue-the-mask'
Vue.use(VueTheMask)
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
Vue.use(VueSweetalert2);

Vue.component('v-slider', require('./components/Content/Calculator.vue').default);
Vue.component('v-gallery', require('./components/Content/AboutCompany/Сertificates').default);
Vue.component('v-for-sale', require('./components/Content/Main/PropertiesForSale').default);
Vue.component('v-for-news', require('./components/Content/Main/News').default);

Vue.component('v-menu', require('./components/layouts/Header/Navbar/index.vue').default);
Vue.component('v-links', require('./components/layouts/Footer/Links/index.vue').default);
Vue.component('v-links-mobile', require('./components/layouts/Footer/Mobile/index.vue').default);
Vue.component('v-guarantee', require('./components/Content/AboutCompany/CompanyGuarantee/index.vue').default);
Vue.component('v-my-program', require('./components/Content/Cabinet/MyCabinet/MyProgram/index.vue').default);
Vue.component('v-progress', require('./components/Content/Cabinet/MyCabinet/Progress/index.vue').default);
Vue.component('v-payment-calendar', require('./components/Content/Cabinet/MyCabinet/PaymentCalendar/index.vue').default);
Vue.component('v-agreement', require('./components/Content/Agreement/index.vue').default);
Vue.component('v-announcement', require('./components/Content/Cabinet/Announcement/index.vue').default);
Vue.component('v-programs', require('./components/Content/Main/Programs/index.vue').default);
Vue.component('v-preimushchestva', require('./components/Content/AboutCompany/OurAdvantages').default);
Vue.component('v-vacancy-filter', require('./components/Content/CompanyVacancies/Filter').default);
Vue.component('v-obyavleniya-filter', require('./components/Content/AllAds/FIlter').default);
Vue.component('v-ads', require('./components/Content/AllAds/Ads').default);
Vue.component('v-vacancies', require('./components/Content/CompanyVacancies/Vacancies').default);
Vue.component('v-news', require('./components/Content/News').default);
Vue.component('v-programs-2', require('./components/Content/AllPrograms').default);
Vue.component('v-voprosy-otvety', require('./components/Content/FAQ').default);
Vue.component('v-faq-form', require('./components/Content/FAQ/Form').default);
Vue.component('v-faq-sform', require('./components/Content/FAQ/SForm').default);
Vue.component('v-graph', require('./components/Content/AccumulationSystem').default);
Vue.component('v-ad', require('./components/Content/Ad/index.vue').default);
Vue.component('v-next-payment', require('./components/Content/Cabinet/MyCabinet/NextPayment/index.vue').default);
Vue.component('v-static', require('./components/Content/Cabinet/MyCabinet/Static/index.vue').default);
Vue.component('v-cabinet-news', require('./components/Content/Cabinet/MyCabinet/News/index.vue').default);
Vue.component('pagination', require('laravel-vue-pagination'));
Vue.component('layout-slider', require('./components/Content/Complex/LayoutsSlider').default);
Vue.component('complex-form', require('./components/Content/Complex/ComplexForms').default);
Vue.component('modals', require('./components/Content/Complex/Modals').default);

// Vue.component('v-faq-collapse', require('./components/FaqComponent.vue').default);

Vue.component('v-burger-menu', require('./components/layouts/Header/Navbar/Burger.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

(function() {
    'use strict';
    window.addEventListener('load', function() {
// Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
// Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

var alertList = document.querySelectorAll('.alert')
alertList.forEach(function (alert) {
    alert.addEventListener('closed.bs.alert', function () {
        let id = alert.getAttribute('data-id');
        fetch(`/warning/dismiss/${id}`);
    })
})

function scrollWin() {
    window.scrollBy(100, 0);
}
