@extends('layouts.app', ['show_menu' => true])

@section('content')

    <section class="complex-info">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="complex_info-desc">
                        <div class="complex-logo">
                            <img src="/img/complex/logo_complex.png" alt="">
                        </div>
                        <div class="complex-head">
                            Новый жилой комплекс <br>
                            в уютном районе г. Шымкент
                        </div>
                        <div class="complex-additional">
                            Поторопись стать обладателем прекрасной квартиры <br> по выгодной стоимости!
                        </div>
                        <div class="complex-btn">
                            <a href="#complex-layouts" class=" btn btn-blue_light">
                                Выбрать квартиру
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 position-relative">
                    <div class="complex-image_under-info">
                        <div class="under_text">
                            <div class="about-under">
                                Старт продаж
                            </div>
                            <div class="about-date">
                                01.09.2022 г.
                            </div>
                            <div class="about-step">
                                1 очередь, 1-2 дома
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="complex-image">
            <img src="/img/complex/complex1.jpg" alt="complex_image">
        </div>
    </section>

    <section class="complex-numbers">
        <div class="container">
            <div class="complex_numb-title">
                О проекте в цифрах
            </div>
            <div class="complex-numb-desc">
                Уникальный жилой комплекс премиум класса, расположенный в одном <br>
                из лучших городов Казахстана
            </div>
            <div class="row">
                <div class="col-6 col-md-4">
                    <div class="complex_numb-item">
                        <div class="numb_item">
                            5
                        </div>
                        <div class="numb_desc">
                            жилых этажей
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="complex_numb-item">
                        <div class="numb_item">
                            14
                        </div>
                        <div class="numb_desc">
                            жилых зданий
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-4">
                    <div class="complex_numb-item">
                        <div class="numb_item">
                            2
                        </div>
                        <div class="numb_desc">
                            класс жилья
                        </div>
                    </div>
                </div>
                <div class="col-6 col-md-6">
                    <div class="complex_numb-item">
                        <div class="numb_item">
                            252
                        </div>
                        <div class="numb_desc">
                            квартиры
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="complex_numb-item">
                        <div class="numb_item">
                            6,5
                        </div>
                        <div class="numb_desc">
                            сейсмостойкость
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="complex-location">
        <div class="location-slot">
            <div class="loc_slot-left">
                <img src="/img/complex/complex2.jpg" alt="">
            </div>
            <div class="loc_slot-right">

            </div>
        </div>

        <div class="location-info">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-6">

                    </div>
                    <div class="col-12 col-md-6">
                        <div class="location_info-title">
                            Расположение
                        </div>
                        <div class="location_info-desc">
                            <p>Жилой комплекс “Amanat DELUXE” расположен в экологически чистом районе города
                                Шымкент.</p>
                            <p>Расположение жилого комлпекса позволит жителям жить вдали от городской загазованности.
                                Развитая инфраструктура позволяет с комфортом проживать в данной части города для людей
                                любых возрастов.</p>
                            <p>Сервисная компания обеспечивает безопасное и бесперебойное функционирование жилого
                                комплекса
                                24/7.</p>
                        </div>
                        <div class="location_info-btn">
                            @php
                            $link = json_decode(setting('site.delux'), true)[0]['download_link'];
                            @endphp
                            <a target="_blank" :href="'storage/'+{{json_encode($link)}}" class=" btn btn-blue_light">
                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none"
                                     xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M17.9326 10.9395C17.8111 10.671 17.5441 10.5 17.2501 10.5H14.2501V0.75C14.2501 0.336 13.9141 0 13.5001 0H10.5001C10.0861 0 9.75009 0.336 9.75009 0.75V10.5H6.75009C6.45609 10.5 6.18909 10.6725 6.06759 10.9395C5.94459 11.208 5.99259 11.5215 6.18609 11.7435L11.4361 17.7435C11.5786 17.907 11.7841 18 12.0001 18C12.2161 18 12.4216 17.9055 12.5641 17.7435L17.8141 11.7435C18.0091 11.523 18.0541 11.208 17.9326 10.9395Z"
                                        fill="white"/>
                                    <path
                                        d="M20.25 16.5V21H3.75V16.5H0.75V22.5C0.75 23.3295 1.422 24 2.25 24H21.75C22.5795 24 23.25 23.3295 23.25 22.5V16.5H20.25Z"
                                        fill="white"/>
                                </svg>
                                Скачать презентацию
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </section>


    <section class="complex_form-1">
        <div class="container">
            <div class="complex-form1_wrapper">
                <div class="row">
                    <div class="col-12 col-md-8">
                        <div class="complex_form-title">
                            Закажите консультацию
                        </div>
                        <div class="complex_form-desc">
                            Наш менеджер ответит на все Ваши вопросы и поможет подобрать Вам квартиру мечты!
                        </div>
                    </div>
                    <div class="col-12 col-md-4">
                        <div class="complex_form-title_phone">
                            +7 (747) 111 62 91
                        </div>
                        <div class="complex_form-desc text-right">
                            Или позвоните нам сами!
                        </div>
                    </div>
                </div>
                <complex-form></complex-form>
            </div>
        </div>
    </section>

    <section class="why_amanat">
        <div class="container">
            <div class="why_amanat-title">
                Почему Amanat DELUXE?
            </div>
            <div class="why_amanat-desc">
                Amanat DELUXE это не только красиво, это также Ваше комфортное будущее
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="why_amanat-item">
                        <div class="why_item-img">
                            <img src="/img/complex/why1.png" alt="">
                        </div>
                        <div class="why_item-title">
                            Красивый город
                        </div>
                        <div class="why_item-desc">
                            ЖК расположен в городе Шымкент
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="why_amanat-item">
                        <div class="why_item-img">
                            <img src="/img/complex/why2.png" alt="">
                        </div>
                        <div class="why_item-title">
                            Район
                        </div>
                        <div class="why_item-desc">
                            Респектабельный район города Шымкент
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="why_amanat-item">
                        <div class="why_item-img">
                            <img src="/img/complex/why3.png" alt="">
                        </div>
                        <div class="why_item-title">
                            Инфраструктура
                        </div>
                        <div class="why_item-desc">
                            Очень развитая инфраструктура
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="why_amanat-item">
                        <div class="why_item-img">
                            <img src="/img/complex/why4.png" alt="">
                        </div>
                        <div class="why_item-title">
                            Уютный двор
                        </div>
                        <div class="why_item-desc">
                            Уютный двор с ландшафтным дизайном
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="why_amanat-item">
                        <div class="why_item-img">
                            <img src="/img/complex/why5.png" alt="">
                        </div>
                        <div class="why_item-title">
                            Пожарная безопасность
                        </div>
                        <div class="why_item-desc">
                            Современная система пожарной безопасности
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="why_amanat-item">
                        <div class="why_item-img">
                            <img src="/img/complex/why6.png" alt="">
                        </div>
                        <div class="why_item-title">
                            Паркинг
                        </div>
                        <div class="why_item-desc">
                            Охраняемый паркинг для автомобиля с видеонаблюдением
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="complex-layouts">
        <div class="container">
            <div id="complex-layouts" class="layouts-title">
                Планировки
            </div>
            <div class="layouts-desc">
                Для получения подробной информации Вам необходимо обратить в офис продаж
            </div>

            <layout-slider> </layout-slider>


        </div>
    </section>

    <section class="complex_form-2">
        <div class="container">
            <div class="complex_form2-wrapper">
                <div class="complex_form2-title">
                    Выбрали удобную планировку?
                </div>
                <div class="complex-form2-desc">
                    Оставьте заявку и мы обсудим все детали!
                </div>
                <complex-form></complex-form>

            </div>
        </div>
    </section>


    <section class="about-developer">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="developer_summary">
                        <div class="dev_summary-logo">
                            <img src="/img/complex/AH.svg" alt="">
                        </div>
                        <div class="dev-summary-desc">
                            <strong>20 лет</strong> успешной работы <br>
                            Более <strong>1 640 000</strong> клиентов по всему Казахстану
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="developer-info">
                        <div class="dev_info-title">
                            О застройщике
                        </div>
                        <div class="dev_info-desc">
                            <p>Жилищный кооператив «Amanat Home» - осуществляет свою деятельность в соответствии с
                                законом
                                Республики Казахстан «О жилищных отношениях» №94-I от 16 апреля 1997 года и «О
                                некоммерческих организациях» №142 от 16 января 2001 года, согласно которых единственным
                                учредительным документом Жилищного кооператива является его Устав, утвержденный на
                                собрании
                                пайщиков.</p>
                            <p>
                                Устав является юридическим основанием всех действий Жилищного кооператива и его органов
                                как
                                во внутренней деятельности, так и в отношениях с третьими лицами.</p>
                            <p>
                                То есть наша деятельность абсолютно прозрачна и отражена в Уставе, который представлен
                                на
                                нашем сайте наряду с остальными основополагающими документами Кооператива.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @if(session('success'))
        <modals :status="1"></modals>
    @elseif(session('error'))
        <modals :status="2"></modals>
    @endif


@endsection

