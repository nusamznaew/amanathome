@extends('layouts.app', ['show_menu' => true])

@section('content')
        <v-news news="{{ json_encode($news) }}"></v-news>
@endsection
