@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb pl-0 mb-md-5">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="/news">Новости</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $news->slug }}</li>
            </ol>
        </nav>
    </div>
    <div class="container">
        <p class="subtitle">Новости</p>
        <div class="d-flex flex-column">
            <p class="title">
                {{$news->title}}
            </p>
            <p class="date">
                {{$news->created_at_formated}}
            </p>
            <p class="text">
                {!! $news->text !!}
            </p>
        </div>
    </div>
@endsection
