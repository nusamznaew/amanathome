@extends('layouts.cabinet',['payload'=>json_decode($payload)])

@section('content')
    <div class="d-flex mt20">
        <v-agreement agreements="{{ json_encode($agreements) }}"/>
    </div>
@endsection
