@extends('layouts.cabinet')

@section('content')
    <div class="d-flex flex-column announcement">
        <p class="mt20 title">Мои объявления</p>
        @if ($ads->count() > 0)
        <p class="text">У вас {{ $ads->count() }} обьявления</p>
        @endif
    </div>
    <v-announcement ads="{{ json_encode($ads) }}"></v-announcement>
    @if ($ads->count() == 0 && $user->available_ads == 0)
    <p class="null-ads">
        Если хотите разместить объявление, свяжитесь с нами по электронной почте
        <a href="mailto:{{ setting('site.email') }}" class="email-ads">{{ setting('site.email') }}</a>
    </p>
    @elseif($ads->count() == 0 && $user->available_ads != 0)
    <p class="null-ads">
        Чтобы разместить объявление, перейдите по <a href="{{ route('ad.create') }}" class="email-ads">ссылке</a>
    </p>
    @endif
@endsection

@push('styles')
<style>
.null-ads {
    color: #d6d6d6;
    font-size: 26px;
    text-align: center;
    padding: 25px 0;
}
.email-ads {
    color: #ea7c3b;
}
</style>
@endpush