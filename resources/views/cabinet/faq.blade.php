@extends('layouts.cabinet',['payload'=>json_decode($payload)])

@section('content')
   <v-voprosy-otvety message="{{ session('message') }}" questions="{{ json_encode($questions) }}"/>
@endsection
