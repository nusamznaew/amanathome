@extends('layouts.cabinet',['payload'=>json_decode($payload)])

@section('content')
    <div class="container mt-3">
        <div class="row justify-content-center">
            <div class="col-sm-8">
                <div class="card p-3 bsd">
                    <form action="{{ route('cabinet.update') }}" method="post">
                        @csrf
                        <div class="mb-2">
                            <x-label for="old_password" :value="__('Старый пароль')" class="m-0"/>

                            <x-input id="old_password" class="form-control block mt-1 w-full" type="password" name="old_password" :value="old('old_password')" required autofocus />
                        </div>
                        @error('old_password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="my-2">
                            <x-label for="password" :value="__('Новый пароль')" class="m-0"/>

                            <x-input id="password" class="form-control block mt-1 w-full" type="password" name="password" :value="old('password')" required autofocus />
                        </div>
                        @error('password')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="my-2">
                            <x-label for="password_confirmation" :value="__('Подтвердите пароль')" class="m-0"/>

                            <x-input id="password_confirmation" class="form-control block mt-1 w-full" type="password" name="password_confirmation" :value="old('password_confirmation')" required autofocus />
                        </div>
                        <input type="submit" class="btn btn-orange d-md-block d-none" value="Обновить">
                        @if (session('message') == 'success')
                        <div class="alert alert-success">
                            Пароль успешно сменен 
                        </div>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
    <style>
    .bsd {
        background: #fff;
        box-shadow: 0 4px 17px rgb(11 11 77 / 15%);
        border-radius: 8px;
    }
    </style>
@endsection
