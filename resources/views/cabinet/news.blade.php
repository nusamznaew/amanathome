@extends('layouts.cabinet',['payload'=>json_decode($payload)])

@section('content')
        <v-cabinet-news message="{{ session('message') }}" news="{{ json_encode($news) }}"></v-cabinet-news>
@endsection

