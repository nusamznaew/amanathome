@extends('layouts.cabinet',['payload'=>$payload])

@section('content')
    <div class="cabinet-wrap">
        <div class="cabinet_back-button">
            <a href="{{ route('cabinet.news') }}"> <img src="/img/icons/Vector.png" alt="" class="cabinet_icon-img"> Назад </a>
        </div>
        <div class="cabinet_news-container">
            <div class="cabinet_news-title flex-row mb-3 align-items-center d-flex justify-content-between">
                <p class="m-0 h2">{{ $newsItem->title }}</p>
                <p class="date m-0 lh-1 h2 text-right"><b>Дата:</b><br>{{ $newsItem->created_at_formated }}</p>
            </div>
            <div class="topic-img" >
                <img src="/storage/{{ $newsItem->image }}" alt="">
            </div>
            <div class="cabinet_news-text">
                <p>{!!  $newsItem->text !!}</p>
            </div>
        </div>
    </div>
@endsection

