@extends('layouts.cabinet', ['payload'=>json_decode($payload)])

@section('content')
    <div class="row">

        <v-my-program payload="{{ $payload }}"></v-my-program>
        <v-progress payload="{{ $payload }}"></v-progress>
    </div>
    <div class="row">
        <v-next-payment payload="{{ $payload }}"></v-next-payment>
        <v-static payload="{{ $payload }}"></v-static>
    </div>

    <v-payment-calendar payload="{{ $payload }}"></v-payment-calendar>
@endsection
