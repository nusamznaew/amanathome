@extends('layouts.app', ['show_menu' => true])

@section('content')
    <div class="vacancies">
        @include('partials.banner')
        <div id="content"></div>
        <v-vacancies vacancy="{{ json_encode($vacancy) }}"></v-vacancies>
    </div>
    @include('modals.vacancy')
@endsection
