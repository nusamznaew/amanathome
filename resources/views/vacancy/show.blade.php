@extends('layouts.app')

@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb pl-0 mb-md-5">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="/news">Вакансии</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $vacancy->name }}</li>
            </ol>
        </nav>
    </div>
    <div class="container vacancy-info">
        <div class="d-flex flex-column">
            <div class="d-flex flex-md-row flex-column">
                <p class="vacancy-name mr-auto">{{ $vacancy->name }}</p>
                <p class="vacancy-salary mr-3">от {{ number_format($vacancy->salary, 0, ","," ") }} тг/мес.</p>
                <a href="" class="btn send-cv d-md-flex d-none" data-bs-toggle="modal" data-bs-target="#leaveVacancyRequestModal">Отправить резюме</a>
            </div>
            <div class="d-flex align-items-center">
                <p class="employer">Amanathome</p>
                <p class="date">{{ $vacancy->created_at_formated }}</p>
            </div>
            <div class="d-flex flex-md-row flex-column options">
                <p>
                    <svg width="20" height="20">
                        <use xlink:href="/img/sprite.svg#grid"></use>
                    </svg>
                    {{ $vacancy->position }}
                </p>

                <p>
                    <svg width="20" height="20">
                        <use xlink:href="/img/sprite.svg#briefcase"></use>
                    </svg>
                    {{ $vacancy->experience }}
                </p>
                <p>
                    <svg width="20" height="20">
                        <use xlink:href="/img/sprite.svg#clock"></use>
                    </svg>
                    {{ $vacancy->employment_type }}
                </p>
                <p>
                    <svg width="20" height="20">
                        <use xlink:href="/img/sprite.svg#pointer"></use>
                    </svg>
                    {{ $vacancy->location }}
                </p>
            </div>
            <div class="d-flex description">
                {{ $vacancy->text }}
            </div>
        </div>
        <a href="" class="btn send-cv d-md-none d-flex mt40">Отправить резюме</a>
    </div>
    @include('modals.vacancy')
@endsection
