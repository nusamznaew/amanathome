@extends('layouts.app', ['show_menu' => true])

@section('content')
    <div class="main">
        <div class="block1 d-flex align-items-center position-relative">
            <div class="d-lg-none d-block fon position-absolute"></div>
            <div class="container position-relative">
                <div class="d-flex flex-column">
                    <h1 class="text-blue">Amanat Home</h1>
                    <p class="text rty123">Приобретите недвижимость в рассрочку
                        <span>до 10 лет</span> вместе с Amanat Home! </p>
                    <a class="btn mt50 btn-orange-outline" href="#programms" onclick="scrollWin()">Узнать больше</a>
                </div>
            </div>
        </div>
        <div id="sliders"></div>
        <v-slider></v-slider>
        <div class="container mt100 programs" id="programms">
            <a href="/all-programms" class="subtitle">Все программы</a>
            <div class="d-flex flex-column mt-3">
                <v-programs payload="{{ json_encode($tariffs) }}"/>
            </div>
        </div>
        <v-for-sale ads="{{ json_encode($ad) }}"></v-for-sale>
        <v-for-news news="{{ json_encode($news) }}"></v-for-news>
    </div>
@endsection
