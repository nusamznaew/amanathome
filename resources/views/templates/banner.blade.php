{!! view(
    [
        'template' => $banner->content
    ],
    [
        'banner' => $banner,
        'programms' => $programms,
        'questions' => $questions
    ]
) !!}