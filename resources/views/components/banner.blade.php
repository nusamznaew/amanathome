    <div class="d-flex flex-column">
        <h1 class="text-blue">{!! $banner->title !!}</h1>
        {!! $banner->description !!}
        <a onclick="scrollWin()" class="btn mt50 btn-orange-outline" href="#content">{!! $banner->button !!}</a>
    </div>
