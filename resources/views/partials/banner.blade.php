<div class="block1 d-flex align-items-center position-relative">
    <div class="d-lg-none d-block fon position-absolute"></div>
    <div class="container position-relative">
        @include('components.banner', ['components.banner' => $banner])
    </div>
</div>
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb pl-0">
            <li class="breadcrumb-item"><a href="/">Главная</a></li>
            <li class="breadcrumb-item active" aria-current="page">{!! $banner->title !!}</li>
        </ol>
    </nav>
</div>
