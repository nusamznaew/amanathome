@extends('layouts.app', ['show_menu' => true])

@section('content')
    <div class="ads">
        <div class="block1 d-flex align-items-center position-relative">
            <div class="d-md-none d-block fon position-absolute"></div>
            <div class="container position-relative">
                @include('components.banner', ['components.banner' => $banner])
            </div>
        </div>
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb pl-0">
                    <li class="breadcrumb-item"><a href="/">Главная</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Все объявления</li>
                </ol>
            </nav>
        </div>
<div id="content"></div>
        <v-obyavleniya-filter></v-obyavleniya-filter>
        <div class="block3 container mt50">
            <p class="subtitle">Недвижимость в продаже</p>
            <v-ads ads="{{ json_encode($ad) }}"></v-ads>
        </div>
    </div>
@endsection
