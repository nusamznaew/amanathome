@extends('layouts.app')

@section('content')
    <div class="forms">
        <div class="container">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb pl-0">
                    <li class="breadcrumb-item"><a href="/">Главная</a></li>
                    <li class="breadcrumb-item"><a href="/ads">Все объявления</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Подать объявление</li>
                </ol>
            </nav>
        </div>
<div id="content"></div>
        <div class="d-flex flex-md-row flex-column justify-content-center container align-items-start block2">
            @if (!Auth::check())
                <div class="form mr-5">
                    <form class="needs-validation" method="POST">
                        <p class="title">Подать объявление</p>
                        <div class="d-flex justify-content-center">
                            <svg width="247" height="62" viewBox="0 0 247 62" fill="none"
                                 xmlns="http://www.w3.org/2000/svg">
                                <rect x="52" y="30" width="177" height="3" fill="#CCCCCC"/>
                                <circle cx="31" cy="31" r="31" fill="#E97B3B" fill-opacity="0.7"/>
                                <circle cx="31" cy="32" r="22" fill="#E97B3B"/>
                                <circle cx="225" cy="32" r="22" fill="#CCCCCC"/>
                                <path
                                    d="M33.6145 24.4545H30.7239L27.123 26.7344V29.4616L30.454 27.3736H30.5392V39H33.6145V24.4545Z"
                                    fill="white"/>
                                <path
                                    d="M219.88 40H230.249V37.4858H224.142V37.3864L226.265 35.3054C229.255 32.5781 230.058 31.2145 230.058 29.5597C230.058 27.0384 227.998 25.2557 224.88 25.2557C221.826 25.2557 219.731 27.081 219.738 29.9361H222.657C222.65 28.544 223.531 27.6918 224.859 27.6918C226.137 27.6918 227.089 28.4872 227.089 29.7656C227.089 30.9233 226.379 31.7188 225.058 32.9901L219.88 37.7841V40Z"
                                    fill="white"/>
                            </svg>
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Ваше имя <span
                                    style="color: red">*</span></label>
                            <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Введите имя"
                                   required>
                            <div class="invalid-feedback">
                                Обязательное поле
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Ваш e-mail</label>
                            <input type="email" class="form-control" id="exampleFormControlInput1"
                                   placeholder="Введите e-mail">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Номер телефона <span
                                    style="color: red">*</span></label>
                            <input type="tel" class="form-control" id="exampleFormControlInput1" placeholder="+7" required>
                            <div class="invalid-feedback">
                                Обязательное поле
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlInput1" class="form-label">Придумайте пароль <span
                                    style="color: red">*</span></label>
                            <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Пароль"
                                   required>
                            <div class="invalid-feedback">
                                Обязательное поле
                            </div>
                        </div>
                        <label for="validationCustom04" class="form-label">Выберите регион <span
                                style="color: red;">*</span></label>
                        <select class="form-select" id="validationCustom04" required>
                            <option selected disabled value="">Алматы</option>
                            <option>...</option>
                        </select>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                        <div class="form-check d-flex  align-items-center mt20">
                            <input class="form-check-input mr-1 mt-0" type="checkbox" value="" id="flexCheckChecked"
                                   required>
                            <label class="form-check-label checkbox-label" for="flexCheckChecked">
                                Я принимаю условия <span> пользовательского соглашения</span>
                            </label>
                        </div>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                        <button type="submit" class="btn btn-orange mt30">Далее &nbsp;&rarr;</button>
                        <hr>
                        <div class="d-flex justify-content-center">
                            <a href="#">Авторизоваться</a>
                        </div>
                    </form>
                </div>
            @endif
            @if (!session('message'))
                <div class="form">
                <form class="needs-validation" action="/ad" method="POST">
                    @csrf
                    <p class="title">Подать объявление</p>
                    <div class="d-flex justify-content-center">
                        <svg width="247" height="62" viewBox="0 0 247 62" fill="none"
                             xmlns="http://www.w3.org/2000/svg">
                            <rect x="43" y="28" width="177" height="3" fill="#CCCCCC"/>
                            <circle cx="216" cy="31" r="31" fill="#E97B3B" fill-opacity="0.7"/>
                            <circle cx="22" cy="30" r="22" fill="#CCCCCC"/>
                            <circle cx="216" cy="30" r="22" fill="#E97B3B"/>
                            <path
                                d="M24.6145 22.4545H21.7239L18.123 24.7344V27.4616L21.454 25.3736H21.5392V37H24.6145V22.4545Z"
                                fill="white"/>
                            <path
                                d="M210.88 38H221.249V35.4858H215.142V35.3864L217.265 33.3054C220.255 30.5781 221.058 29.2145 221.058 27.5597C221.058 25.0384 218.998 23.2557 215.88 23.2557C212.826 23.2557 210.731 25.081 210.738 27.9361H213.657C213.65 26.544 214.531 25.6918 215.859 25.6918C217.137 25.6918 218.089 26.4872 218.089 27.7656C218.089 28.9233 217.379 29.7188 216.058 30.9901L210.88 35.7841V38Z"
                                fill="white"/>
                        </svg>
                    </div>
                    <div class="mb-3">
                        <label for="category_id" class="form-label">
                            Выберите категорию
                            <span style="color: red;">*</span>
                            @error('category_id')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <select class="form-select" name="category_id" id="category_id" required>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->name }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="room_amount" class="form-label">
                            Количество комнат
                            <span style="color: red">*</span>
                            @error('room_amount')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <input type="text" class="form-control" name="room_amount" id="room_amount"
                               placeholder="Количество комнат" required>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="type_id" class="form-label">
                            Тип строения
                            @error('type_id')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <select class="form-select" name="type_id" id="type_id">
                            @foreach($types as $type)
                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                            @endforeach
                        </select>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="built_year" class="form-label">
                            Год постройки
                            <span style="color: red">*</span>
                            @error('built_year')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <input type="text" class="form-control" name="built_year" id="built_year"
                               placeholder="Год постройки"
                               required>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="level_amount" class="form-label">
                            Количество уровней
                            <span style="color: red">*</span>
                            @error('level_amount')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <input type="text" class="form-control" name="level_amount" id="level_amount"
                               placeholder="Количество уровней"
                               required>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3 volume">
                        <label class="form-label">
                            Площадь, м²
                            @error('general_area')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            @error('living_area')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            @error('kitchen_area')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <div class="d-flex justify-content-between align-items-end">
                            <div class="d-flex flex-column">
                                <label for="general_area" class="form-label lil-label">
                                    Общая
                                    <span style="color: red">*</span>
                                </label>
                                <input type="text" class="form-control" name="general_area" id="general_area"
                                       placeholder="Общая"
                                       required>
                            </div>
                            <div class="d-flex flex-column">
                                <input type="text" class="form-control" name="living_area" id="living_area"
                                       placeholder="Жилая">
                            </div>
                            <div class="d-flex flex-column">
                                <input type="text" class="form-control" name="kitchen_area" id="kitchen_area"
                                       placeholder="Кухня">
                            </div>
                        </div>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="plot_size" class="form-label">
                            Площадь участка, соток
                            <span style="color: red">*</span>
                            @error('plot_size')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <input type="text" class="form-control" name="plot_size" id="plot_size"
                               placeholder="Площадь участка, соток"
                               required>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="address" class="form-label">
                            Адрес недвижимости
                            @error('address')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <input type="text" class="form-control" name="address" id="address"
                               placeholder="Адрес недвижимости">
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="is_pledged" class="form-label">
                            В залоге
                            @error('is_pledged')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <select class="form-select" name="is_pledged" id="is_pledged">
                            <option selected disabled value="">Нет</option>
                            <option>...</option>
                        </select>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="description" class="form-label">
                            Описание объявления
                            @error('description')
                            <span class="invalid-feedback d-inline-block" role="alert">
                            <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </label>
                        <textarea class="form-control" name="description" id="description" rows="3"
                                  placeholder="Описание...">
                        </textarea>
                    </div>
                    <button type="submit" class="btn btn-orange mt30">Отправить объявление &nbsp;&rarr;</button>
                    <hr>
                    <div class="d-flex justify-content-center">
                        <a href="#" style="color:#CCCCCC;">Назад</a>
                    </div>
                </form>
            </div>
            @endif
        </div>
        @if (session('message') && session('message') == 'created')
        <div class="d-flex flex-column align-items-center justify-content-center mt40 block3">
            <svg width="83" height="63" viewBox="0 0 83 63" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                    d="M25.3169 63C24.0557 63 22.9345 62.4285 22.2339 61.5716L1.07361 38.1429C-0.467875 36.4287 -0.327592 33.7143 1.35391 32.1428C3.03541 30.5714 5.69808 30.7144 7.23957 32.4286L25.4569 52.5714L75.9052 1.28564C77.5867 -0.428546 80.2493 -0.428546 81.7908 1.28564C83.3323 2.99982 83.4723 5.71424 81.7908 7.28568L28.2598 61.7143C27.5589 62.5715 26.438 63 25.3169 63Z"
                    fill="#E97B3B"/>
            </svg>
            <p class="title">Объявление успешно опубликовано!</p>
            <p class="text">Наш менеджер свяжется с Вами в ближайшее
                время для уточнения деталей</p>
            <button class="btn btn-orange">Вернуться на главную</button>
        </div>
        @elseif (session('message'))
        <div class="alert alert-danger" role="alert">
            {{ session('message') }}
        </div>
        @endif
    </div>
@endsection
