@extends('layouts.app')
@section('content')
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb pl-0 mb-md-5">
                <li class="breadcrumb-item"><a href="/">Главная</a></li>
                <li class="breadcrumb-item"><a href="/news">Новости</a></li>
                <li class="breadcrumb-item active" aria-current="page">Карточка обьяления</li>
            </ol>
        </nav>
    </div>
    <div class="container card-info">
        <div class="d-flex flex-md-row flex-column">
            <div class="_left">
                <v-ad ad="{{ $ad->images }}"></v-ad>
            </div>
            <div class="d-flex flex-column _right">
                <p class="title">{{ $ad->room_amount }}-комнатная квартира, {{ $ad->general_area }} м², {{
                    $ad->level }}/{{ $ad->level_amount }} этаж, {{ $ad->address }}</p>
                <p class="price">{{ number_format($ad->price, 0, ","," ") }} тг</p>
{{--                {{ dd(json_decode($ad->images)) }}--}}
                <ul class="info">
                    <li><span>Адрес</span> ............................................. {{ $ad->address }}</li>
                    <li><span>Дом</span> ................................................... {{ $ad->building_type }}</li>
                    <li><span>Этаж</span> ................................................. {{ $ad->level }} из {{ $ad->level_amount }}</li>
                    <li><span>Площадь</span> ..................................... {{ $ad->general_area }}  м², кухня — {{ $ad->kitchen_area }}  м²</li>
                    <li><span>Состояние</span> ................................. {{ $ad->state }}</li>
                    <li><span>Санузел</span> ........................................ {{ $ad->bathroom }}</li>
                </ul>
                <a class="btn" href="tel:{{ setting('site.phone') }}">Позвонить</a>
            </div>
        </div>
        <div class="d-flex description">
            <p>
                {!! $ad->description !!}
            </p>
        </div>
    </div>
@endsection
