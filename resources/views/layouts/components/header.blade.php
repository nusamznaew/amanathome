<header>
    <div class="container header d-flex align-items-center justify-content-lg-center justify-content-between">
        <div>
            <v-burger-menu
                class="d-lg-none d-block"
                items="{{ json_encode(menu('site', '_json')->toArray()) }}"
                login-url="{{ route('login') }}"
                logo="/storage/{{ setting('site.logo') }}"
            />
        </div>
        <a class="navbar-brand mr-sm-3 mr-0" href="/">
            <img
                src="/storage/{{ setting('site.logo') }}"
                class="d-inline-block align-top"
                alt="logo"
            />
        </a>
        <a href="#" class="d-lg-none d-block" data-bs-toggle="modal" data-bs-target="#callModal">
            <svg width="21" height="21">
                <use xlink:href="/img/sprite.svg#phone"></use>
            </svg>
        </a>
        <a href="/branches-and-terminals" class="text1 mr-auto d-lg-block d-none">
            <img src="/img/icons/pointer.svg" alt="pointer.svg">
            Отделения и терминалы
        </a>
        <div class="d-flex flex-column mr-3  d-lg-block d-none">
            <a href="tel: {{ setting('site.phone') }}"><p class="tel">{{ setting('site.phone') }}</p></a>
            <a href="#" class="order-call" data-bs-toggle="modal" data-bs-target="#leaveRequestModal">
                Заказать звонок
            </a>
        </div>
        <a href="{{ route('login') }}" class="btn login btn-orange d-lg-block d-none text-nowrap">
            @auth
                Перейти в кабинет
            @endauth
            @guest
                Войти
            @endguest
        </a>
    </div>
    <div class="menu d-lg-block d-none">
        @includeWhen(isset($show_menu) && $show_menu, 'partials.menu')
    </div>
</header>
