<footer class="mt60">
    <div class="container d-lg-flex d-none flex-column">
        <v-links
            items="{{ json_encode(menu('site', '_json')->toArray()) }}"
            site="{{ json_encode([
                'name' => setting('site.title'),
                'logo' => setting('site.footer_logo'),
                'address' => setting('site.address'),
                'gis' => setting('site.gis'),
                'phone' => setting('site.phone'),
                'vkontakte' => setting('site.vkontakte'),
                'youtube' => setting('site.youtube'),
                'instagram' => setting('site.instagram'),
                'facebook' => setting('site.facebook')
            ]) }}"
        />
    </div>
    <v-links-mobile
        items="{{ json_encode(menu('site', '_json')->toArray()) }}"
        site="{{ json_encode([
            'name' => setting('site.title'),
            'logo' => setting('site.footer_logo'),
            'address' => setting('site.address'),
            'gis' => setting('site.gis'),
            'phone' => setting('site.phone'),
            'vkontakte' => setting('site.vkontakte'),
            'youtube' => setting('site.youtube'),
            'instagram' => setting('site.instagram'),
            'facebook' => setting('site.facebook'),
            'social_margin_y' => setting('sitestyles.social_margin_y'),
        ]) }}"
    />
</footer>
