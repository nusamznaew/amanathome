<script src="{{ asset('js/app.js') }}" defer></script>
{!! setting('site.scripts') !!}
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.min.js" defer></script>
<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js" defer></script>
@stack('scripts')
