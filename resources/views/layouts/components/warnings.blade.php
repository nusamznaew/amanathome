@php
$warnings = \App\Models\Warning::active()->get();
$session = json_decode(session('warnings'), true);
if (!$session) {
    $session = [];
}
@endphp
<div class="position-fixed w-100 warnings-wrapper p-3 pb-0">
@foreach($warnings as $warning)
    @if (!in_array($warning->id, $session))
    <div class="alert alert-{{ $warning->type }} alert-dismissible fade show" data-id="{{ $warning->id }}">
        {!! $warning->content !!}
        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
    </div>
    @endif
@endforeach
</div>