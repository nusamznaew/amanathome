<!doctype html>
<html lang="en">
<head>
    @include('layouts.components.metas')
    @include('layouts.components.styles')
    <title>{{ setting('site.title') }}</title>
</head>
<body>
    <div id="app">
        @include('layouts.components.header')
        @yield('content')
        <div class="mt60">
            @include('layouts.components.footer')
        </div>
        @include('layouts.components.warnings')
        @include('modals.apply')
        @include('modals.phone')
    </div>
    @include('layouts.components.scripts')
</body>
</html>
