@extends('layouts.app', ['show_menu' => true])

@section('content')
    <div class="faq">
        @include('partials.banner')
        <div id="content"></div>
        <v-voprosy-otvety message="{{ session('message') }}" questions="{{ json_encode($questions) }}"></v-voprosy-otvety>
    </div>
@endsection