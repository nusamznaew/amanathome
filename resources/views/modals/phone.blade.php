<div class="modal fade" id="callModal" tabindex="-1" aria-labelledby="callModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="d-flex flex-column align-items-center modal-body">
                <button type="button" class="btn-close align-self-end mb-3" data-bs-dismiss="modal" aria-label="Close"></button>
                <a href="tel: {{ setting('site.phone') }}" class="number">{{ setting('site.phone') }}</a>
                <a href="#" data-bs-toggle="modal" data-bs-target="#leaveRequestModal" class="order-call">Заказать звонок</a>
            </div>
        </div>
    </div>
</div>
