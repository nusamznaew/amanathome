<div class="modal fade" id="leaveVacancyRequestModal" tabindex="-1" aria-labelledby="leaveVacancyRequestModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="leaveVacancyRequestModalLabel">Оставьте нам заявку</h5>
                <button type="button" class="btn-close align-self-end mb-3" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form action="{{ route('vacancy.apply') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="d-flex flex-column">
                        <label for="name">Ваше ФИО</label>
                        <input class="form-control" type="text" id="name_vacancy" name="name" placeholder="Введите имя" required>
                        
                        <label class="mt-3" for="email_vacancy">Ваш e-mail</label>
                        <input class="form-control" type="email" id="email_vacancy" name="email" placeholder="Введите e-mail" required>
                        <label class="mt-3" for="phone_vacancy">Номер телефона</label>
                        <the-mask class="form-control" placeholder="+7" name="phone" id="phone_vacancy" :mask="['+7 (###)-###-##-## ']" required></the-mask>
                        <label class="mt-3" for="file_vacancy">Ваше резюме</label>
                        <input class="form-control" type="file" id="file_vacancy" name="file" accept=".doc,.docx,.pdf" required>
                        
                        <div class="form-check mt-4 pl-0">
                            <label class="form-check-label" for="flexCheckDefault">
                                Наш менеджер свяжется с Вами в ближайшее время
                            </label>
                        </div>
                        {!! NoCaptcha::display() !!}
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn w-100">Отправить
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                  d="M14.4715 8.4714C14.5354 8.40749 14.5836 8.33382 14.6162 8.25519C14.6488 8.17658 14.6667 8.09039 14.6667 8C14.6667 7.90961 14.6488 7.82341 14.6162 7.74481C14.5836 7.66618 14.5354 7.59251 14.4715 7.52859L10.7002 3.75736C10.4399 3.49701 10.0178 3.49701 9.75744 3.75736C9.49709 4.01771 9.49709 4.43982 9.75744 4.70017L12.3906 7.33333H2.00008C1.63189 7.33333 1.33341 7.63181 1.33341 8C1.33341 8.36819 1.63189 8.66667 2.00008 8.66667H12.3906L9.75744 11.2998C9.49709 11.5602 9.49709 11.9823 9.75744 12.2426C10.0178 12.503 10.4399 12.503 10.7002 12.2426L14.4715 8.4714Z"
                                  fill="white"/>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-4">
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                {!! implode('', $errors->all('<div>:message</div>')) !!}
            </div>
            @endif

            @if(session('message'))
            <div class="alert alert-success" role="alert">
                {{ session('message') }}
            </div>
            @endif
        </div>
    </div>
</div>

@push('scripts')
{!! NoCaptcha::renderJs() !!}
@endpush