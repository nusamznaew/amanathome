@extends('layouts.app')

@section('content')
    <div class="forms mt50">
        <div class="d-flex flex-md-row flex-column justify-content-center container align-items-start block2">
            <div class="form mr-5">
                <form class="needs-validation" novalidate>
                    <p class="title">Регистрация</p>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Ваше ФИО</label>
                        <input type="text" class="form-control" id="exampleFormControlInput1" placeholder="Введите имя"
                               required>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Ваш e-mail</label>
                        <input type="email" class="form-control" id="exampleFormControlInput1" placeholder="Введите e-mail">
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Номер телефона</label>
                        <input type="tel" class="form-control" id="exampleFormControlInput1" placeholder="+7" required>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Придумайте пароль</label>
                        <input type="password" class="form-control" id="exampleFormControlInput1" placeholder="Пароль"
                               required>
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                    </div>
                    <label for="validationCustom04" class="form-label">Выберите регион</label>
                    <select class="form-select" id="validationCustom04" required>
                        <option selected disabled value="">Алматы</option>
                        <option>...</option>
                    </select>
                    <div class="invalid-feedback">
                        Обязательное поле
                    </div>
                    <div class="form-check d-flex  align-items-center mt20">
                        <input class="form-check-input mr-1 mt-0" type="checkbox" value="" id="flexCheckChecked" required>
                        <label class="form-check-label checkbox-label" for="flexCheckChecked">
                            Я принимаю условия <span> пользовательского соглашения</span>
                        </label>
                    </div>
                    <div class="invalid-feedback">
                        Обязательное поле
                    </div>
                    <button type="submit" class="btn btn-orange mt30">Далее &nbsp;&rarr;</button>
                    <hr>
                    <div class="d-flex justify-content-center">
                        <a href="#">Авторизоваться</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
