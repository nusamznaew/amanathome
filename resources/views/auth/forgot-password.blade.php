@extends('layouts.app', ['show_menu' => false])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-sm-8">
            <x-guest-layout>
                <x-auth-card>
                    <x-slot name="logo">
                        <a href="/">
                            <x-application-logo class="d-none w-20 h-20 fill-current text-gray-500" />
                        </a>
                    </x-slot>

                    <div class="mb-4 text-sm text-gray-600">
                        {{ __('Возниклы проблемы со входом? Мы отправим Вам новый пароль на Ваш телефон если Вы есть в нашей базе') }}
                    </div>

                    <!-- Session Status -->
                    <x-auth-session-status class="mb-4" :status="session('status')" />

                    <!-- Validation Errors -->
                    <x-auth-validation-errors class="mb-4" :errors="$errors" />

                    <form method="POST" action="{{ route('password.phone') }}">
                        @csrf

                        <!-- Phone Address -->
                        <div>
                            <x-label for="phone" :value="__('Телефон')" />

                            <x-input id="phone" class="form-control block mt-1 w-full" type="phone" name="phone" :value="old('phone')" required autofocus />
                        </div>

                        <div class="flex items-center justify-end mt-4">
                            <x-button class="btn btn-orange">
                                {{ __('Сбросить') }}
                            </x-button>
                        </div>
                    </form>
                </x-auth-card>
            </x-guest-layout>
        </div>
    </div>
</div>
@endsection