@extends('layouts.app', ['show_menu' => false])

@section('content')
    <div class="forms mt50">
        <div class="d-flex flex-md-row flex-column justify-content-center container align-items-start block2">
            <div class="form mr-5">
                <form method="POST" class="needs-validation" action="{{ route('login') }}">
                    @csrf
                    <p class="title">Вход</p>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Логин</label>
                        <input
                            type="text"
                            class="form-control @error('login') is-invalid @enderror"
                            id="exampleFormControlInput1"
                            value="{{ old('login') }}"
                            placeholder="Введите имя"
                            required
                            name="login"
                        >
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                        @error('login')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="mb-3">
                        <label for="exampleFormControlInput1" class="form-label">Пароль</label>
                        <input
                            type="password"
                            class="form-control  @error('password') is-invalid @enderror"
                            id="exampleFormControlInput1"
                            placeholder="Пароль"
                            required
                            name="password"
                        >
                        <div class="invalid-feedback">
                            Обязательное поле
                        </div>
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-orange mt30">Войти &nbsp;&rarr;</button>
                    <div class="d-flex justify-content-center">
                        <a class="forgot-password" href="{{ route('password.request') }}">Забыли пароль?</a>
                    </div>
                    <div class="d-flex justify-content-center mt40">
                        <p class="no-account">Еще нет аккаунта? <a href="">Зарегистрируйтесь</a></p>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
