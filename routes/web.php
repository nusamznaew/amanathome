<?php

use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\{
    PageController,
    ApplyController,
    AdController,
    NewsController,
    VacancyController,
    CabinetController,
    QuestionController,
    MainController
};
use App\Http\Controllers\API\{AuthController, NewsController as APINewsController, ProgramController, UserController};
use Illuminate\Support\Facades\Route;
use TCG\Voyager\Facades\Voyager;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [MainController::class, 'index']);

Route::get('complex', [MainController::class, 'complex']);

Route::post('/request/complex', [MainController::class, 'complexRequest'])->name('request.complex');

Route::prefix('ad')->name('ad.')->group(function () {
    Route::get('/', [AdController::class, 'index'])
        ->name('index');

    Route::get('create', [AdController::class, 'create'])
        ->name('create');

    Route::post('/', [AdController::class, 'store'])
        ->middleware('auth');

    Route::get('{ad}', [AdController::class, 'show'])
        ->name('show');
});

Route::prefix('faq')->group(function () {
    Route::get('/', [QuestionController::class, 'index'])
        ->name('faq');

    Route::post('/', [QuestionController::class, 'store']);
});

Route::prefix('news')->group(function () {
    Route::get('/', [NewsController::class, 'index'])
        ->name('news');

    Route::get('{news}', [NewsController::class, 'show']);
});

Route::prefix('vacancy')->name('vacancy.')->group(function () {
    Route::get('/', [VacancyController::class, 'index'])
        ->name('index');

    Route::post('apply', [VacancyController::class, 'notify'])
        ->name('apply');

    Route::get('{vacancy}', [VacancyController::class, 'show'])
        ->name('show');
});

Route::prefix('cabinet')->middleware('auth')->name('cabinet.')->group(function () {
    Route::get('/', [CabinetController::class, 'index'])
        ->name('index');
    Route::get('edit', [CabinetController::class, 'edit'])
        ->name('edit');

    Route::post('update', [CabinetController::class, 'update'])
        ->name('update');

    Route::get('agreement', [CabinetController::class, 'showAgreement'])
        ->name('agreement');

    Route::prefix('news')->group(function () {
        Route::get('/', [CabinetController::class, 'indexNews'])
            ->name('news');

        Route::get('/{newsItem}', [CabinetController::class, 'showNews']);
    });

    Route::get('announcement', [CabinetController::class, 'showAnnouncement'])
        ->name('announcement');

    Route::get('faq', [CabinetController::class, 'showFaq'])
        ->name('faq');
});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('view-news', [APINewsController::class, 'viewNews'])->name('api.view.news');
Route::get('read-news', [APINewsController::class, 'readNews'])->name('api.read.news');



Route::post('apply', [ApplyController::class, 'create']);

Route::get('how-to-pay', [PageController::class, 'howToPay']);
Route::get('{banner}', [PageController::class, 'banner'])->name('banner');

Route::get('warning/dismiss/{id}', [PageController::class, 'warningDismiss'])->name('warning-dismiss');

Route::get('page/{page}', [PageController::class, 'page'])->name('page');

Route::get('static/{page}', [PageController::class, 'staticPage'])->name('static');

Route::get('clear-cache', function () {
    $exitCode = Artisan::call('optimize');
    return '<h1>Clear Config cleared</h1>';
});

