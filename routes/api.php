<?php

use App\Http\Controllers\API\{AuthController, NewsController, ProgramController, UserController};
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', [AuthController::class, 'login']);
Route::get('user', [UserController::class, 'index'])->middleware('auth:api');
Route::post('user', [UserController::class, 'store'])->middleware('auth:api');
Route::post('users/delete', [UserController::class, 'batchDelete'])->middleware('auth:api');
Route::post('users', [UserController::class, 'storeBatch'])->middleware('auth:api');
Route::get('programs', [ProgramController::class, 'index']);
