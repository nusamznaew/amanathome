<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ClientImported extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    private $password;
    private $login;

    public function __construct($password, $login)
    {
        $this->password = $password;
        $this->login = $login;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Клиент создан')
            ->greeting('Здравствуйте!')
            ->line("Мы рады Вам сообщить, что Вы были добавлены в нашу базу клиентов.")
            ->line("Ваш логин: $this->login.")
            ->line("Ваш пароль: $this->password.")
            ->line("Перейдите по ссылке, чтобы войти в аккаунт")
            ->action('Войти', route('login'))
            ->line('Спасибо что выбрали нас!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
