<?php

namespace App\Http\Controllers;

use App\Models\{ Question, Banner, User };
use Illuminate\Http\Request;
use App\Notifications\QuestionAsked;
use App\Http\Requests\StoreQuestion;

class QuestionController extends Controller
{
    public function index()
    {
        $questions = Question::all();
        $banner = Banner::where('slug', 'faq')->first();
        return view('faq.index', compact('banner', 'questions'));
    }

    public function store(StoreQuestion $request)
    {
        $data = $request->validated();
        $proccessed = [
            'ФИО' => $data['full_name'],
            'Почта' => $data['tel'],
            'Номер Телефона' => $data['phone'],
            'Город' => $data['city'],
        ];
        $ad = Question::create($data);
        $users = User::whereHas('role', function ($query) {
            return $query->where('name', 'manager');
        })->get();
        foreach ($users as $user) {
            $user->notify(new QuestionAsked($proccessed));
        }
        return back()->with('message', 'Ваш запрос отправлен');
    }
}
