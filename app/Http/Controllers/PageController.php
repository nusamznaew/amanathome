<?php

namespace App\Http\Controllers;

use App\Models\{ Banner, Tariff, Page, Question };
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PageController extends Controller
{
    public function banner(Banner $banner)
    {
        $questions = Question::all();
        $programms = Tariff::all();
        return view('templates.banner', compact('banner', 'questions', 'programms'));
    }

    public function howToPay()
    {
        $banner = Banner::where('slug', 'how-to-pay')->first();
        $questions = Question::all();
        $programms = Tariff::all();
        return view('templates.howToPay', compact('banner', 'questions', 'programms'));
    }

    public function page(Page $page)
    {
        return view($page->template, compact('page'));
    }

    public function staticPage($slug)
    {
        return view('templates.'.$slug);
    }

    public function warningDismiss($id)
    {
        if (is_numeric($id)) {
            $warnings = json_decode(session('warnings'), true);
            if (!$warnings) {
                $warnings = [];
            }
            if (!in_array($id, $warnings)) {
                $warnings[] = $id;
                session(['warnings' => json_encode($warnings)]);
                return 0;
            }
        }
    }
}
