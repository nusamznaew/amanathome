<?php

namespace App\Http\Controllers;

use App\Models\{ Ad, News, Banner, Tariff };
use App\Http\Controllers\Mail\Complex;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Throwable;

class MainController extends Controller
{
    public function index () {

        $ad = Ad::all();
        $news = News::latest('created_at')->get();
        $tariffs = Tariff::orderBy('order', 'asc')->get();

        $ad->map(function ($item, $key){
            $item->images = json_decode($item->images);
            return $item;
        });

        return view('main', compact('ad', 'news', 'tariffs'));
    }

    public function complex () {
        return view('complex.index');
    }

    public function complexRequest(Request $request)
    {
        $data = $request->except(['_token']);
        $name = $data['name'];
        $phone = $data['phone'];
        $city = $data['city'];

        try {
            Mail::to(setting('site.email'))->send(new Complex($name, $phone, $city,));
            return Redirect::back()->with('success', 'message sent');
        } catch (Throwable $e) {
            return Redirect::back()->with('error', 'message not sent');
        }

    }

}
