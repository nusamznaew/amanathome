<?php

namespace App\Http\Controllers;

use App\Models\{ Banner, Vacancy, User };
use Illuminate\Http\Request;
use App\Http\Requests\SendApply;
use App\Notifications\ApplySent;

class VacancyController extends Controller
{
    public function index(Request $request) {
        $filters = $request->all();
        $banner = Banner::whereSlug('vacancy')->first();
        $vacancy = Vacancy::query();

        foreach ($filters as $key => $value) {
            if (strpos($value, ',')) {
                $value = explode(',', $value);
            }
            if (gettype($value) == 'string') {
                if ($key == 'finder') {
                    $key = 'name';
                    $value = '%'.preg_replace('/[-,\s]/', '%', $value).'%';
                    $vacancy = $vacancy->where($key, 'LIKE', $value);
                } else {
                    $vacancy = $vacancy->where($key, $value);
                }
            } else if (gettype($value) == 'array') {
                $vacancy = $vacancy->whereBetween($key, $value);
            }
        }

        $vacancy = $vacancy->get();

        return view('vacancy.index', compact('vacancy', 'banner'));
    }

    public function show(Vacancy $vacancy) {
        return view('vacancy.show', compact('vacancy'));
    }

    public function notify(SendApply $request)
    {
        $data = $request->validated();
        $proccessed = [
            'ФИО' => $data['name'],
            'Почта' => $data['email'],
            'Номер Телефона' => $data['phone'],
            'Файл' => isset($data['file']) ? $data['file'] : null
        ];
        $user = User::where('email', 'ali.ongarbekovich@gmail.com')->first();
        $user->notify(new ApplySent($proccessed));
        return back()->with('message', 'Ваша заявка успешно отправлена');
    }
}
