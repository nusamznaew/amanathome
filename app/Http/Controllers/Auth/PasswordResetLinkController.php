<?php

namespace App\Http\Controllers\Auth;

use Formatter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\Models\User;
use App\Notifications\ClientCreated;

class PasswordResetLinkController extends Controller
{
    /**
     * Display the password reset link request view.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('auth.forgot-password');
    }

    /**
     * Handle an incoming password reset link request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $request->validate([
            'phone' => 'required',
        ]);
    
        $user = User::where('phone', Formatter::phone($request->get('phone')))->firstOrFail();

        $password = Str::random(8);
        $user->password = Hash::make($password);
        $user->save();
        $user->notify(new ClientCreated($password, $user->phone));
    
        return redirect()->route('login');
    }
}
