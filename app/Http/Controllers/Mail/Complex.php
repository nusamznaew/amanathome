<?php

namespace App\Http\Controllers\Mail;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Complex extends Mailable
{
    use Queueable, SerializesModels;

    protected $name;
    protected $phone;
    protected $city;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $phone, $city)
    {

        $this->name = $name;
        $this->phone = $phone;
        $this->city = $city;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mailVoyager = setting('site.email');
        return $this->from('info@amanathome.kz')
            ->to($mailVoyager)
            ->view('mail.mail')
            ->with([
                'name' => $this->name,
                'phone' => $this->phone,
                'city' => $this->city,
            ])
            ->subject('Новая заявка c сайта Amanat Delux');
    }
}
