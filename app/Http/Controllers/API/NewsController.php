<?php


namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{

    public function readNews(Request $request)
    {
        $request->validate([
            'id' => ['integer', 'exists:news,id']
        ]);
        $viewedNewsIDS = Auth::user()->viewNews->pluck('id')->toArray();
        if (!in_array($request->id, $viewedNewsIDS)) {
            Auth::user()->viewNews()->sync($request->id);
            $news = News::find($request->id);
        }
        return response()->json([
            'news' => $news
        ]);
    }

    public function viewNews(Request $request)
    {
        $request->validate([
            'type' => ['in:new,read']
        ]);
        if ($request->type === 'read') {
            $data = Auth::user()->viewNews()->paginate(10);
        } else {
            $viewedNewsIDS = Auth::user()->viewNews()->pluck('id')->toArray();
            $data = News::whereNotIn('id', $viewedNewsIDS)->paginate(10);
        }
        return response()->json($data);
    }

}
