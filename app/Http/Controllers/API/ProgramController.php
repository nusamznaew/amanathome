<?php

namespace App\Http\Controllers\API;

use App\Models\Program;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProgramController extends Controller
{
    public function index() {
        return Program::all();
    }
}
