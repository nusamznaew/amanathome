<?php

namespace App\Http\Controllers\API;

use Formatter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ User };
use App\Notifications\ClientCreated;
use App\Http\Requests\{ StoreUser, StoreUsers };
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function index()
    {
        return User::all();
    }
    
    public function store(StoreUser $request)
    {
        $data = $request->validated();
        $password = '123456'; // Str::random(8)
        $data['password'] = Hash::make($password);
        $data['phone'] = Formatter::phone($data['phone']);
        $user = User::create($data);
        $user->notify(new ClientCreated($password, $user->phone));

        return response()->json(['message' => 'created', 'payload' => $user]);
    }

    public function storeBatch(StoreUsers $request)
    {
        $validated = $request->users;
        $users = [];
        foreach ($validated as $data) {
            $password = Str::random(8);
            $data['password'] = Hash::make($password);
            $data['phone'] = Formatter::phone($data['phone']);

            $user = User::create($data);
            $user->notify(new ClientCreated($password, $user->phone));
            $users[] = $user;
        }
        return response()->json(['message' => 'created', 'payload' => $users]);
    }

    public function batchDelete(Request $request)
    {
        $data = $request->validate([
            'users' => 'array|required',
            'users.*' => 'integer|exists:users,id'
        ]);

        User::whereIn('id', $data['users'])->delete();
        return ['message' => 'ok'];
    }
}
