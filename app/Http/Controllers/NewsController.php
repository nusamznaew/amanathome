<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{
    public function index() {
        $banner = Banner::whereSlug('news')->first();
        $news = News::all();

        return view('news.index', compact('news', 'banner'));
    }

    public function show(News $news) {
            $news = News::find($news->id);
        return view('news.show', compact('news'));
    }
}
