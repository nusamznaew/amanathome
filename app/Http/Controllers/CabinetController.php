<?php

namespace App\Http\Controllers;

use App\Facades\API;
use Illuminate\Http\Request;
use App\Models\News;
use  Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CabinetController extends Controller
{
    public function index () {
        $plan = API::handlePlan();
        $client = API::handleClient($plan);
        $schedule = API::handleSchedule($plan, $client);
        $loan = API::handleLoan($client, $schedule, $plan);

        $payload = json_encode([
            'client' => $client,
            'loan' => $loan,
            'schedule' => $schedule
        ]);

//        dd($payload);

        return view('cabinet.index', compact('payload'));
    }

    public function edit()
    {
        $plan = API::handlePlan();
        $client = API::handleClient($plan);
        $schedule = API::handleSchedule($plan, $client);
        $loan = API::handleLoan($client, $schedule, $plan);

        $payload = json_encode([
            'client' => $client,
            'loan' => $loan,
            'schedule' => $schedule
        ]);
        $user = Auth::user();
        return view('cabinet.edit', compact('user', 'payload'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'old_password' => ['required'],
            'password' => ['required', 'string', 'min:8', 'confirmed']
        ]);
        $user = Auth::user();
        
        $password = Str::random(8);
        $user->password = Hash::make($password);
        $user->save();
        return back()->with('message', 'success');
    }

    public function showAgreement () {
        $plan = API::handlePlan();
        $client = API::handleClient($plan);
        $schedule = API::handleSchedule($plan, $client);
        $loan = API::handleLoan($client, $schedule, $plan);

        $payload = json_encode([
            'client' => $client,
            'loan' => $loan,
            'schedule' => $schedule
        ]);
        $agreements = [];
        foreach($client as $key => $value){
            $exp_key = explode('_', $key);
            if ($exp_key[0] == 'document') {
                $agreements[$exp_key[1]] = $value;
            }
        }
        return view('cabinet.agreement', compact('agreements','payload'));
    }

    public function showAnnouncement () {
        $user = Auth::user();
        $ads = $user->ads;
        return view('cabinet.announcement', compact('ads', 'user'));
    }

    public function showFaq () {
        $plan = API::handlePlan();
        $client = API::handleClient($plan);
        $schedule = API::handleSchedule($plan, $client);
        $loan = API::handleLoan($client, $schedule, $plan);

        $payload = json_encode([
            'client' => $client,
            'loan' => $loan,
            'schedule' => $schedule
        ]);
        $questions = Question::all();
        return view('cabinet.faq', compact('questions','payload'));
    }

    public function indexNews()
    {
        $payload = json_encode([]);

        $news = News::all();

        return view('cabinet.news.index', compact('news', 'payload'));
    }

    public function showNews($news) {
        $news = News::where('slug', $news)->first();
        $payload = [];
        $viewedNewsIDS = Auth::user()->viewNews->pluck('id')->toArray();
        if (!in_array($news->id, $viewedNewsIDS)) {
            $viewedNewsIDS[] = $news->id;
            Auth::user()->viewNews()->sync($viewedNewsIDS);
            $news = News::find($news->id);
        }
        $newsItem = $news;
        return view('cabinet.news.show', compact('newsItem', 'payload'));
    }

}
