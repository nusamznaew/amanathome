<?php

namespace App\Http\Controllers;
use App\Models\User;
use Illuminate\Http\Request;
use App\Notifications\ApplySent;
use App\Http\Requests\SendApply;

class ApplyController extends Controller
{
    public function create(SendApply $request)
    {
        $data = $request->validated();
        $proccessed = [
            'ФИО' => $data['name'],
            'Почта' => $data['email'],
            'Номер Телефона' => $data['phone'],
            'Город' => $data['city'],
        ];
        $users = User::whereHas('role', function ($query) {
            return $query->where('name', 'manager');
        })->get();
        foreach ($users as $user) {
            $user->notify(new ApplySent($proccessed));
        }
        return back()->with('message', 'Ваша заявка успешно отправлена');
    }
}
