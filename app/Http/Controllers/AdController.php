<?php

namespace App\Http\Controllers;

use App\Models\{ Ad, Banner, Category, Type, Vacancy };
use Illuminate\Http\Request;
use App\Http\Requests\StoreAd;

class AdController extends Controller
{
    public function index (Request $request) {
        $filters = $request->all();
        $banner = Banner::whereSlug('ad')->first();
        $ad = Ad::query();

        foreach ($filters as $key => $value) {
            if (strpos($value, ',')) {
                $value = explode(',', $value);
            }

            if (gettype($value) == 'string') {
                $ad = $ad->where($key, $value);
            } else if (gettype($value) == 'array') {
                $ad = $ad->whereBetween($key, $value);
            }
        }
        $ad = $ad->get();
        $ad->map(function ($item, $key){
            $item->images = json_decode($item->images);
            return $item;
        });
        return view('ads.index', compact('ad', 'banner'));
    }

    public function create () {
        $categories = Category::all();
        $types = Type::all();
        return view('ads.create', compact('categories', 'types'));
    }

    public function store (StoreAd $request) {
        $data = $request->validated();
        if (Auth::user()->available_ads >= 0) {
            $data['user_id'] = \Auth::id();
            $ad = Ad::create($data);
            return back()->with('message', 'created');
        }
        return back()->with('message', 'У вас нет доступа');
    }

    public function show ($id) {
        $ad = Ad::find($id);
        return view('ads.show', compact('ad'));
    }
}
