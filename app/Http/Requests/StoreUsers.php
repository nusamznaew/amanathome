<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUsers extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'users' => ['array'],
            'users.*.name' => ['required'],
            'users.*.email' => ['required', 'unique:users', 'email'],
            'users.*.phone' => ['required', 'string', 'max:12'],
            'users.*.iin' => ['required', 'digits:12'],
            'users.*.region' => ['required', 'string']
        ];
    }
}
