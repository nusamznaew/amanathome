<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendApply extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'g-recaptcha-response' => ['nullable', 'captcha'],
            'name' => ['string', 'required'],
            'email' => ['string', 'required', 'email'],
            'phone' => ['string', 'required'],
            'file' => ['file', 'nullable', 'mimes:pdf,docx,doc'],
        ];
    }
}
