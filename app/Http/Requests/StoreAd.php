<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return \Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => ['required', 'integer'],
            'room_amount' => ['required', 'integer'],
            'type_id' => ['required', 'integer'],
            'built_year' => ['required', 'integer', 'digits:4', 'max:'.(date('Y') + 1)],
            'level_amount' => ['required', 'integer'],
            'general_area' => ['required', 'integer'],
            'living_area' => ['required', 'integer'],
            'kitchen_area' => ['required', 'integer'],
            'plot_size' => ['required_if:type_id,1', 'integer'],
            'address' => ['nullable', 'string'],
            'description' => ['nullable', 'string'],
            'is_pledged' => ['nullable', 'boolean']
        ];
    }

    public function messages()
    {
        return [
            'room_amount' => 'Поле количество комнат является обязательным.',
            'type_id' => 'Поле тип строения является обязательным.',
            'built_year' => 'Поле год постройки является обязательным.',
            'built_year.max' => 'Год постройки не может быть больше :max года',
            'built_year.digits' => 'Год постройки должен состоять из :digits цифр.',
            'level_amount' => 'Поле количество уровней является обязательным.',
            'general_area.required' => 'Поле общая площадь является обязательным.',
            'living_area.required' => 'Поле жилая площадь является обязательным.',
            'kitchen_area.required' => 'Поле кухонная площадь является обязательным.',
            'plot_size.required' => 'Поле площадь участка, соток является обязательным.',
            'address.required' => 'Поле адрес недвижимости является обязательным.',
            'description.required' => 'Поле описание объявления является обязательным.',
        ];
    }
}
