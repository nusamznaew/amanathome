<?php

namespace App\Observers;

use TCG\Voyager\Models\User;
use App\Notifications\{ ClientCreated, ClientImported };

class UserObserver
{
    /**
     * Listen to the User created event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function created(User $user)
    {
        if (\Request::route() != null && \Request::route()->getName() == 'voyager.users.store') {
            try {
		$user = \App\Models\User::find($user->id);
                $user->notify(new ClientCreated($user->hash, $user->phone));
                if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                    $user->notify(new ClientImported($user->hash, $user->phone));
                }
            } catch (\Exception $e) {
                
            }
        }
    }
}
