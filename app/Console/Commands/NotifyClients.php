<?php

namespace App\Console\Commands;

use App\Notifications\{ ClientCreated, ClientImported };
use App\Models\User;
use Illuminate\Console\Command;

class NotifyClients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify clients by email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::whereHas('role', function ($query) {
            return $query->where('name', 'user');
        })->where('hash', '<>', null)->limit(400)->get();
        foreach ($users as $index => $user) {
            try {
                $user->notify(new ClientCreated($user->hash, $user->phone));
                if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                    $user->notify(new ClientImported($user->hash, $user->phone));
                }
            } catch (\Exception $e) {
                
            }
            $user->hash = null;
            $user->save();
        }
        return 0;
    }
}
