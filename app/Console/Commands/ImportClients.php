<?php

namespace App\Console\Commands;

use App\Imports\ClientsModel;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Console\Command;

class ImportClients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:clients';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import clients from xlsx';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Excel::import(new ClientsModel, public_path('/storage/raw.xlsx'));
        return 0;
    }
}
