<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Support\Facades\Http;
use Illuminate\Console\Command;

class CheckIndividualIdentificationNumber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:iin';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check IIN availability';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();
        $troubles = [];

        foreach ($users as $index => $user) {
            $iin = $user->iin;
            $response = Http::get("https://web.prodvig.kz/script/api/flat2.php?iin=$iin");
            echo "$index\n";
            if ($response->status() !== 200) {
                $troubles[] = $user->iin;
            }
        }
        \Log::info(json_encode($troubles));
        return 0;
    }
}
