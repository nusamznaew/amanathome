<?php

namespace App\Console\Commands;

use Formatter;
use App\Models\User;
use Illuminate\Console\Command;

class FormatPhones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'format:phones';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Format phones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $users = User::all();
        foreach ($users as $user) {
            $user->phone = Formatter::phone($user->phone);
            $user->save();
        }
        return 0;
    }
}
