<?php

namespace App\Imports;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Notifications\ClientImported;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class ClientsModel implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $user = User::where('email', $row[3])->first();
        if ($user) {
            var_dump($user->name);
        } elseif ($row[1] && $row[2] && $row[3] && $row[0]) {
            $password = Str::random(8);
            $user = User::firstOrCreate([
                'iin'      => $row[2],
            ], [
                'role_id'  => 2,
                'name'     => $row[0],
                'email'    => $row[3],
                'phone'    => $row[1],
                'password' => Hash::make($password),
                'hash'     => $password
            ]);
            $user->save();
        }
        return $user;
    }
}
