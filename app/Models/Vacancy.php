<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Vacancy extends Model
{

    protected $appends = ['created_at_formated'];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function getCreatedAtFormatedAttribute()
    {
        return $this->created_at->format('d-m-Y');
    }
}
