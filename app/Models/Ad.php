<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{

    use HasFactory;

    public $table = 'houses';

    public $appends = [
        'link'
    ];

    public function getRouteKeyName()
    {
        return 'id';
    }

    public function getLinkAttribute()
    {
        return route('ad.show', $this);
    }
}
