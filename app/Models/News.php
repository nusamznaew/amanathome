<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class News extends Model
{
    protected $appends = ['created_at_formated'];

    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function getCreatedAtFormatedAttribute()
    {
        return $this->created_at->format('d-m-Y');
    }

    public function setSlugAttribute($slug)
    {
        $this->attributes['slug'] = Str::slug($this->title, '-');
    }
}
