<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Warning extends Model
{
    use HasFactory;

    public function scopeActive()
    {
        return $this->where('is_displayed', true)->orderBy('order', 'ASC');
    }
}
