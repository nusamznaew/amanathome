<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class CustomBlade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'custom-blade';
    }
}