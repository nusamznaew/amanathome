<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class API
{
    public function handlePlan()
    {
        $id = \Auth::user()->iin;
        $client_response = Http::get("https://web.prodvig.kz/script/api/flat2.php?iin=$id");
        $client_payload = $client_response->body();
        return json_decode($client_payload, true);
    }

    public function handleClient($plan)
    {
        return isset($plan['Plan30']) ? $plan['Plan30'] : [];
    }

    public function handleLoan($client, $schedule, $plan)
    {
        $date_entry = isset($client['date_entry']) && $client['date_entry'] != 'null' ? $client['date_entry'] : '01.01.2020';
        $entry_date = Carbon::createFromFormat('d.m.Y', substr($date_entry, 0, 10));
        $today = Carbon::today();
        $months_past = $entry_date->diffInMonths($today);
        $next_payment_date = null;
        if ($months_past == sizeof($schedule)) {
            $next_payment_date = $entry_date->addMonths($months_past + 1);
        } else if ($months_past < sizeof($schedule)) {
            $next_payment_date = null;
        }
        $percent_accumulated = 0;
        if (isset($plan['payments_amount']) && isset($client['price'])) {
            $percent_accumulated = round($plan['payments_amount'] / $client['price'] * 100);
        }
//        dd($plan['program_type']);
        if ($plan['program_type'] == 222) {
            $program_type = 'Накопительная';
        } elseif($plan['program_type'] == 224) {
            $program_type = 'Ожидающий покупку';
        } else {
            $program_type = 'Не выбрано';
        }
        
        $months_total = $plan['months'];
        // if (isset($plan['payments70']) && count($plan['payments70']) > 0) {
        //     $months_total = 120;
        // }

        $countdown = 0;
        foreach ($schedule as $row) {
            if ($row['status'] != 'Полностью') {
                $needed_date = Carbon::createFromFormat('d.m.Y', substr($row['period'], 0, 10));
                $countdown = $needed_date->diffInDays($today);
                break;
            }
        }
        return [
            'program_type' => $program_type,
            'payments_amount' => $plan['payments_amount'],
            'planned_date' => $plan['approximate_date_buy'],
            'percent_accumulated' => $percent_accumulated,
            'months_past' => $months_past,
            'months_total' => $months_total,
            'uds' => 100000,
            'next_payment' => $next_payment_date,
            'countdown' => $countdown,
            'queue_max' => $plan['queue_max'],
        ];
    }

    public function handleSchedule($plan, $client)
    {
        $schedule_handled = [];
        $debt = $client['price'] ?? 8000000;
        if (!isset($plan['payments'])) {
            return [];
        }
        if (isset($plan['payments70'])) $plan['payments'] = array_merge($plan['payments'], $plan['payments70']);

        foreach ($plan['payments'] as $payment) {
            $period = $payment['period'];
            $payment_amount = $payment['plan_sum'] ?? 0;
            $payment_actual = $payment['add'] ?? 0;
            $payment_remainder = $payment['remainder'];
            if (in_array($payment['status'], [130, 136])) {
                $status = 'Частично';
            } else if (in_array($payment['status'], [132, 138])) {
                $status = 'Полностью';
            } else {
                $status = 'Запланирован';
            }
            $debt -= $payment_actual;
            $schedule_handled[] = [
                'period' => $period,
                'payment_amount' => intval($payment_amount),
                'payment_actual' => intval($payment_actual),
                'payment_remainder' => round($payment_remainder, 2),
                'status' => $status,
                'debt' => $debt,
                'z' => $payment['rez_fond_plan']
            ];
        }

        return $schedule_handled;
    }
}
