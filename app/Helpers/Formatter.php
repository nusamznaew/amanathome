<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Http;
use Carbon\Carbon;

class Formatter
{
    public function phone($phone)
    {
        if ($phone) {
            $result = preg_replace('/[^0-9]/', '', $phone);
            if (isset($result[0])) {
                if ($result[0] == '8') {
                    $result[0] = '7';
                }
                if (strlen($result) == 11) {
                    $result[0] = '7';
                }
                if (strlen($result) == 10) {
                    $result = '7'. $result;
                }
                $result = preg_replace("/^1?(\d{1})(\d{3})(\d{3})(\d{2})(\d{2})$/", "$1 ($2) $3-$4-$5", $result); // +7 (705) 574-82-88
                $result = '+' . $result;
                return $result;
            }
        }
    }
}
